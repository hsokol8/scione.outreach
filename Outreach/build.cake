#tool nuget:?package=NUnit.ConsoleRunner&version=3.9.0
#tool nuget:?package=OpenCover&version=4.6.519
#tool nuget:?package=ReportGenerator&version=3.1.2
#tool nuget:?package=GitVersion.CommandLine&version=3.6.5
#tool nuget:?package=Cake.Newman&version=0.3.1
#tool nuget:?package=Cake.Bakery&version=0.3.0
#tool nuget:?package=Cake.NuGet&version=0.32.1

#addin nuget:?package=Cake.Newman&version=0.3.1
#addin nuget:?package=Cake.Npm&version=0.16.0
#addin nuget:?package=Cake.FileHelpers&version=3.1.0

#load "./config.cake"

//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");
var environment = Argument("environment", "Dev");
var configuration = Argument("configuration", "Release");

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

var sourceBase = "./";
var buildDir = Directory(sourceBase + "bin/") + Directory(configuration);
var appDir = sourceBase + applicationName + "/";
var appProj = appDir + applicationName + ".sfproj";
var serviceDir = sourceBase + applicationName + "Service/";
var serviceProj = serviceDir + applicationName + "Service.csproj";
var testDir = sourceBase + applicationName + "Service.Tests/";
var testProj = testDir + applicationName + "Service.Tests.csproj";
var integrationTestsBase = sourceBase + applicationName + "Service.IntegrationTests/";

var nugetDir = MakeAbsolute(Directory("./packages"));
var fabricBuildDir = Directory(appDir + "bin") + Directory(configuration);

var codeCoverageResultFile = new FilePath("coverage.xml");
var codeCoverageReportDirectory = Directory("coverage");

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

// Some cleanup tasks fail while the file is locked from VS
Task("Clean")
    .ContinueOnError()
    .Does(() =>
{
    DotNetCoreClean(serviceDir);
    CleanDirectory(nugetDir);
    DotNetCoreClean(testDir);
    CleanDirectory(appDir + "bin/");
    CleanDirectory(appDir + "pkg/");
    CleanDirectory(appDir + "obj/");
});

Task("Restore-NuGet-Packages")
    .IsDependentOn("Clean")
    .Does(() =>
{
    DotNetCoreRestore(serviceDir, new DotNetCoreRestoreSettings { NoCache = true });
	NuGetRestore(appProj, new NuGetRestoreSettings { NoCache = true });
    NuGetInstall(FabricConfig.Package, new NuGetInstallSettings { NoCache = true, OutputDirectory = Directory(nugetDir.FullPath), Version=FabricConfig.Version});
});

Task("Build")
    .IsDependentOn("Restore-NuGet-Packages")
    .IsDependentOn("Version")
    .Does(() =>
{
    DotNetCoreBuild(serviceProj, new DotNetCoreBuildSettings { Configuration = configuration });
    MSBuild(appProj, settings => settings.SetConfiguration(configuration)
		.WithTarget("Package"));
	if(hasUnitTests)
	    DotNetCoreBuild(testProj, new DotNetCoreBuildSettings { Configuration = configuration });
});

Task ("Run-UnitTests")
	.Does(() =>
{
	if (hasUnitTests)
	{
        DotNetCoreTest(testProj, new DotNetCoreTestSettings
        {
            Configuration = configuration,
            NoBuild = true,
			ArgumentCustomization = args => args.Append("--logger \"trx;LogFileName=TestResults.xml\"")
        });
	} else
		Information("Skipping Unit Tests");
});

Task("Run-OpenCover")
	.Does(() =>
{
	var openCoverSettings = new OpenCoverSettings{OldStyle=true, Register="user"}
			.WithFilter($"+[{applicationName}Service]*")
			.WithFilter($"-[{applicationName}ServiceTests]*");
	var testSettings = new DotNetCoreTestSettings
        {
            Configuration = configuration,
            NoBuild = true
        };
	OpenCover(context => context.DotNetCoreTest(testProj, testSettings), codeCoverageResultFile, openCoverSettings);
});

Task("Run-ReportGenerator")
	.IsDependentOn("Run-OpenCover")
	.Does(() =>
{
	ReportGenerator(
		codeCoverageResultFile,
		codeCoverageReportDirectory,
		new ReportGeneratorSettings {
			ReportTypes = new[] { ReportGeneratorReportType.Html }
		}
	);
});

Task("Version")
	.Does(() =>
{
	if (Jenkins.IsRunningOnJenkins)
	{
		GitVersion(new GitVersionSettings{ OutputType = GitVersionOutput.BuildServer });
	}
	var versionInfo = GitVersion();
	Information($"Calculated version: {versionInfo.SemVer}");
		var settings = new XmlPokeSettings
		{
			Namespaces = new Dictionary<string,string>
			{
				{"sf", "http://schemas.microsoft.com/2011/01/fabric"}
			}
		};
		XmlPoke(appDir + "/ApplicationPackageRoot/ApplicationManifest.xml", "/sf:ApplicationManifest/@ApplicationTypeVersion", versionInfo.MajorMinorPatch, settings);
		XmlPoke(appDir + "/ApplicationPackageRoot/ApplicationManifest.xml", "//sf:ServiceManifestRef/@ServiceManifestVersion", versionInfo.MajorMinorPatch, settings);
		XmlPoke(serviceDir + "/PackageRoot/ServiceManifest.xml", "//sf:ServiceManifest/@Version", versionInfo.MajorMinorPatch, settings);
		XmlPoke(serviceDir + "/PackageRoot/ServiceManifest.xml", "//sf:CodePackage/@Version", versionInfo.MajorMinorPatch, settings);
		XmlPoke(serviceDir + "/PackageRoot/ServiceManifest.xml", "//sf:ConfigPackage/@Version", versionInfo.MajorMinorPatch, settings);
		XmlPoke(serviceDir + "/PackageRoot/ServiceManifest.xml", "//sf:EnvironmentVariable[@Name='BUILD_NUMBER']/@Value", versionInfo.FullSemVer, settings);
});

Task("Run-Integration-Tests-QA")
	.Does(() =>
{
	environment="QA";
	RunTarget("Run-Integration-Tests");
});

Task("Run-Integration-Tests")
	.Does(() =>
{
	if(hasIntegrationTests)
	{
		Information("Environment - {0}",environment);
		NpmInstall(settings =>
			settings.AddPackage("newman")
				.InstallGlobally()
		);
		RunCollection($"{integrationTestsBase}SciOne.{applicationName}.Integration.Tests.Full.json",
			settings => settings.DisableStrictSSL()
				.WithEnvironment($"{integrationTestsBase}{jiraCode}Service{environment}.environment.json")
				.UseJsonReporter($"./{applicationName}_IntegrationTests.json")
				.UseJUnitReporter($"./{applicationName}_IntegrationTests.xml")
				.UseCLIReporter()
		);
	} else
		Information("Skipping Integration Tests");
});

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
	.IsDependentOn("Build");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);