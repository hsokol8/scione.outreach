// Grouping used on Proget feeds. Either SM, OF, or SCIONE
env.ProgetGroup='SCIONE'
// Project name
env.PROJ_NAME='Outreach'
// relative path to application manifest file. Should be relative to the root folder of the branch. Must use double back slashes.
env.APP_MANIFEST='Outreach\\ApplicationPackageRoot\\ApplicationManifest.xml'
// relative path to service manifest file. Should be relative to the root folder of the branch. Must use double back slashes.
env.SERVICE_MANIFEST='OutreachService\\PackageRoot\\ServiceManifest.xml'
// BuildMaster application Id, obtain from Build/Release Management. Entered as an INT.
env.BuildMaster_appId=
// BuildMaster deployable Id, obtain from Build/Release Management. Entered as an INT.
env.BuildMaster_deployableId=
// Flag to enable/disable all notifications. Email or Slack. 1 or 0 and entered as a String.
env.Notifications_Enabled='1'
// Flag to enable/disable the execution of Integration Tests. 1 or 0 and entered as a String.
env.run_integrationtests='0'
// list of users for failure email notifications. If blank no emails will be sent.
email_list=''