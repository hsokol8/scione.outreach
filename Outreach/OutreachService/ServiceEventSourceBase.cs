﻿using System;
using System.Diagnostics.Tracing;
using System.Fabric;
using SciOne.Utils.Logging;
using SciOne.Utils.Logging.ServiceTracing;

namespace OutreachService
{
    internal sealed partial class ServiceEventSource : IServiceEventSource
    {
        private readonly string _environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Unknown";
        private ServiceContext _serviceContext;

        [NonEvent]
        public void SetServiceContext(ServiceContext serviceContext) { if (_serviceContext == null) _serviceContext = serviceContext; }

        #region Keywords
        // Event keywords can be used to categorize events. 
        // Each keyword is a bit flag. A single event can be associated with multiple keywords (via EventAttribute.Keywords property).
        // Keywords must be defined as a public class named 'Keywords' inside EventSource that uses them.
        public static class Keywords
        {
            public const EventKeywords Requests = (EventKeywords)0x1L;
            public const EventKeywords ServiceInitialization = (EventKeywords)0x2L;
        }
        #endregion

        #region Events
        // Define an instance method for each event you want to record and apply an [Event] attribute to it.
        // The method name is the name of the event.
        // Pass any parameters you want to record with the event (only primitive integer types, DateTime, Guid & string are allowed).
        // Each event method implementation should check whether the event source is enabled, and if it is, call WriteEvent() method to raise the event.
        // The number and types of arguments passed to every event method must exactly match what is passed to WriteEvent().
        // Put [NonEvent] attribute on all methods that do not define an event.
        // For more information see https://msdn.microsoft.com/en-us/library/system.diagnostics.tracing.eventsource.aspx

        [NonEvent]
        public void Message(string message, params object[] args)
        {
            if (!IsEnabled()) return;
            if (_serviceContext == null)
            {
                var finalMessage = args.Length == 0 ? message : string.Format(message, args);
                Message(
                    Environment.MachineName,
                    _environment,
                    ServiceTracingContext.CorrelationId,
                    ServiceTracingContext.CorrelationDateTime.Ticks == 0 ? "N/A" : $"{ServiceTracingContext.CorrelationDateTime:yyyy-MM-ddTHH:mm:ss.fffffffzzz}",
                    finalMessage);
            }
            else
                ServiceMessage(_serviceContext, message, args);
        }

        private const int MessageEventId = 1;
        [Event(MessageEventId, Level = EventLevel.Informational, Message = "{4}")]
        public void Message(string machineName, string environment, string correlationId, string correlationDateTime, string message)
        {
            if (!IsEnabled()) return;
            WriteEvent(MessageEventId, machineName, environment, correlationId, correlationDateTime, message);
        }

        [NonEvent]
        public void ServiceMessage(string message, params object[] args)
        {
            Message(message, args);
        }

        [NonEvent]
        public void ServiceMessage(ServiceContext serviceContext, string message, params object[] args)
        {
            if (!IsEnabled()) return;
            var finalMessage = args.Length == 0 ? message : string.Format(message, args);
            ServiceMessage(
                serviceContext.ServiceName.ToString(),
                serviceContext.ServiceTypeName,
                GetReplicaOrInstanceId(serviceContext),
                serviceContext.PartitionId,
                serviceContext.CodePackageActivationContext.ApplicationName,
                serviceContext.CodePackageActivationContext.ApplicationTypeName,
                serviceContext.NodeContext.NodeName,
                Environment.MachineName,
                _environment,
                ServiceTracingContext.CorrelationId,
                ServiceTracingContext.CorrelationDateTime.Ticks == 0 ? "N/A" : $"{ServiceTracingContext.CorrelationDateTime:yyyy-MM-ddTHH:mm:ss.fffffffzzz}",
                finalMessage);
        }

        // For very high-frequency events it might be advantageous to raise events using WriteEventCore API.
        // This results in more efficient parameter handling, but requires explicit allocation of EventData structure and unsafe code.
        // To enable this code path, define UNSAFE conditional compilation symbol and turn on unsafe code support in project properties.
        private const int ServiceMessageEventId = 2;
        [Event(ServiceMessageEventId, Level = EventLevel.Informational, Message = "{11}")]
        private
#if UNSAFE
        unsafe
#endif
        void ServiceMessage(
            string serviceName,
            string serviceTypeName,
            long replicaOrInstanceId,
            Guid partitionId,
            string applicationName,
            string applicationTypeName,
            string nodeName,
            string machineName,
            string environment,
            string correlationId,
            string correlationDateTime,
            string message)
        {
#if !UNSAFE
            WriteEvent(ServiceMessageEventId, serviceName, serviceTypeName, replicaOrInstanceId, partitionId, applicationName, applicationTypeName, nodeName, machineName, environment, correlationId, correlationDateTime, message);
#else
            const int numArgs = 12;
            fixed (char* pServiceName = serviceName, pServiceTypeName = serviceTypeName, pApplicationName = applicationName, pApplicationTypeName = applicationTypeName, pNodeName = nodeName, pMachineName = machineName, pEnvironment = environment, pCorrelationId = correlationId, pCorrelationDateTime = correlationDateTime, pMessage = message)
            {
                EventData* eventData = stackalloc EventData[numArgs];
                eventData[0] = new EventData { DataPointer = (IntPtr) pServiceName, Size = SizeInBytes(serviceName) };
                eventData[1] = new EventData { DataPointer = (IntPtr) pServiceTypeName, Size = SizeInBytes(serviceTypeName) };
                eventData[2] = new EventData { DataPointer = (IntPtr) (&replicaOrInstanceId), Size = sizeof(long) };
                eventData[3] = new EventData { DataPointer = (IntPtr) (&partitionId), Size = sizeof(Guid) };
                eventData[4] = new EventData { DataPointer = (IntPtr) pApplicationName, Size = SizeInBytes(applicationName) };
                eventData[5] = new EventData { DataPointer = (IntPtr) pApplicationTypeName, Size = SizeInBytes(applicationTypeName) };
                eventData[6] = new EventData { DataPointer = (IntPtr) pNodeName, Size = SizeInBytes(nodeName) };
                eventData[7] = new EventData { DataPointer = (IntPtr) pMachineName, Size = SizeInBytes(machineName) };
                eventData[8] = new EventData { DataPointer = (IntPtr) pEnvironment, Size = SizeInBytes(environment) };
                eventData[9] = new EventData { DataPointer = (IntPtr) pCorrelationId, Size = SizeInBytes(correlationId) };
                eventData[10] = new EventData { DataPointer = (IntPtr) pCorrelationDateTime, Size = SizeInBytes(correlationDateTime) };
                eventData[11] = new EventData { DataPointer = (IntPtr) pMessage, Size = SizeInBytes(message) };

                WriteEventCore(ServiceMessageEventId, numArgs, eventData);
            }
#endif
        }

        [NonEvent]
        public void ServiceTypeRegistered(int hostProcessId, string serviceType) { ServiceTypeRegistered(hostProcessId, serviceType, _environment); }

        private const int ServiceTypeRegisteredEventId = 3;
        [Event(ServiceTypeRegisteredEventId, Level = EventLevel.Informational, Message = "Service host process {0} registered service type {1}", Keywords = Keywords.ServiceInitialization)]
        private void ServiceTypeRegistered(int hostProcessId, string serviceType, string environment)
        {
            WriteEvent(ServiceTypeRegisteredEventId, hostProcessId, serviceType, environment);
        }

        [NonEvent]
        public void ServiceHostInitializationFailed(string exception) { ServiceHostInitializationFailed(exception, _environment); }

        private const int ServiceHostInitializationFailedEventId = 4;
        [Event(ServiceHostInitializationFailedEventId, Level = EventLevel.Error, Message = "Service host initialization failed", Keywords = Keywords.ServiceInitialization)]
        private void ServiceHostInitializationFailed(string exception, string environment)
        {
            WriteEvent(ServiceHostInitializationFailedEventId, exception, environment);
        }

        // A pair of events sharing the same name prefix with a "Start"/"Stop" suffix implicitly marks boundaries of an event tracing activity.
        // These activities can be automatically picked up by debugging and profiling tools, which can compute their execution time, child activities,
        // and other statistics.
        [NonEvent]
        public void ServiceRequestStart(string requestTypeName) { ServiceRequestStart(requestTypeName, _environment); }

        private const int ServiceRequestStartEventId = 5;
        [Event(ServiceRequestStartEventId, Level = EventLevel.Informational, Message = "Service request '{0}' started", Keywords = Keywords.Requests)]
        private void ServiceRequestStart(string requestTypeName, string environment)
        {
            WriteEvent(ServiceRequestStartEventId, requestTypeName, environment);
        }

        [NonEvent]
        public void ServiceRequestStop(string requestTypeName, string exception = "") { ServiceRequestStop(requestTypeName, exception, _environment); }

        private const int ServiceRequestStopEventId = 6;
        [Event(ServiceRequestStopEventId, Level = EventLevel.Informational, Message = "Service request '{0}' finished", Keywords = Keywords.Requests)]
        private void ServiceRequestStop(string requestTypeName, string exception, string environment)
        {
            WriteEvent(ServiceRequestStopEventId, requestTypeName, exception, environment);
        }
        #endregion

        #region Private methods
        private static long GetReplicaOrInstanceId(ServiceContext context)
        {
            switch (context)
            {
                case StatelessServiceContext stateless:
                    return stateless.InstanceId;
                case StatefulServiceContext stateful:
                    return stateful.ReplicaId;
                default:
                    return -1;
            }
        }
#if UNSAFE
        private int SizeInBytes(string s)
        {
            if (s == null)
            {
                return 0;
            }
            else
            {
                return (s.Length + 1) * sizeof(char);
            }
        }
#endif
        #endregion
    }
}
