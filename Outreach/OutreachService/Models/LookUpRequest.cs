﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutreachService.Models
{
    public class LookUpRequest
    {
        public string PatientPhoneNumber { get; set; }
    }
}
