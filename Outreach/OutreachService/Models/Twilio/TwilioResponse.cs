﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OutreachService.Models.Twilio
{
    public class TwilioResponse
    {
        [JsonProperty(PropertyName = "account_sid")]
        public string AccountSID { get; set; }

        [JsonProperty(PropertyName = "api_version")]
        public string ApiVersion { get; set; }

        [JsonProperty(PropertyName = "error_code")]
        public string ErrorCode { get; set; }

        [JsonProperty(PropertyName = "date_created")]
        public string DateCreated { get; set; }

        [JsonProperty(PropertyName = "error_message")]
        public string ErrorMessage { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "direction")]
        public string Direction { get; set; }
    }
}
