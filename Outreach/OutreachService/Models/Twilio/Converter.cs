﻿using System;

namespace OutreachService.Models.Twilio
{
    public class Converter
    {
        public static string NumberConverter(string patientPhoneNumber)
        {
            if (patientPhoneNumber == null)
                return null;

            char[] charArr = patientPhoneNumber.ToCharArray();

            if (charArr[0] == '+' && charArr[1] == '1' && charArr.Length == 12)
                return patientPhoneNumber;
            else if (charArr[0] == '1' && charArr.Length == 11)
            {
                patientPhoneNumber = "+" + patientPhoneNumber;
                return patientPhoneNumber;
            }
            else if (charArr.Length == 10 && charArr[0] != '0' && charArr[0] != '1')
            {
                patientPhoneNumber = "+1" + patientPhoneNumber;
                return patientPhoneNumber;
            }
            else
            {
                return null;
            }
        }

        public static Boolean CheckMeesageLength(string bodyMessage)
        {
            if (bodyMessage == null)
                return false;

            if (bodyMessage.Length < 1599)
                return true;
            else
                return false;
        }

        public static SMSResponse ConvertResponse(SMSResponse response, TwilioResponse twilioResponse)
        {
            response.accountSID = twilioResponse.AccountSID;
            response.apiVersion = twilioResponse.ApiVersion;
            response.errorCode = twilioResponse.ErrorCode;
            response.errorMessage = twilioResponse.ErrorMessage;
            response.dateCreated = twilioResponse.DateCreated;
            response.status = twilioResponse.Status;
            response.direction = twilioResponse.Direction;

            return response;
        }

        public static LookUpResponse ConvertResponse(LookUpResponse response, LUResponse twilioResponse)
        {
            response.numberType = twilioResponse.Carrier.Type;
            response.errorCode = twilioResponse.Carrier.ErrorCode;
            response.carrierName = twilioResponse.Carrier.Name;
            response.callerName = twilioResponse.CallerName;
            response.countryCode = twilioResponse.CountryCode;

            return response;
        }
    }
}
