﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OutreachService.Models.Twilio
{
    public class LUResponse
    {
        [JsonProperty(PropertyName = "carrier")]
        public Carriers Carrier { get; set; }

        [JsonProperty(PropertyName = "caller_name")]
        public string CallerName { get; set; }

        [JsonProperty(PropertyName = "country_code")]
        public string CountryCode { get; set; }

        public partial class Carriers
        {
            [JsonProperty(PropertyName = "name")]
            public string Name { get; set; }
        }

        public partial class Carriers
        {
            [JsonProperty(PropertyName = "type")]
            public string Type { get; set; }
        }

        public partial class Carriers
        {
            [JsonProperty(PropertyName = "error_code")]
            public string ErrorCode { get; set; }
        }

        public partial class Carriers
        {
            [JsonProperty(PropertyName = "mobile_network_code")]
            public string MobileNetworkCode { get; set; }
        }
    }
}
