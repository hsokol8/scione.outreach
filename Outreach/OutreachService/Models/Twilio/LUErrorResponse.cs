﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OutreachService.Models.Twilio
{
    public class LUErrorResponse
    { 
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "more_info")]
        public string MoreInfo { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
    }
}
