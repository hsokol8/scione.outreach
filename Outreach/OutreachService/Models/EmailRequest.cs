﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutreachService.Models
{
    public class EmailRequest
    {
        public string PatientEmail { get; set; }
        public string MessageBody { get; set; }
        public string EmailSubject { get; set; }
    }
}
