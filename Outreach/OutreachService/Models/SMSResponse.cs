﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SciOne.Utils.Errors.Response;

namespace OutreachService.Models
{
    public class SMSResponse: ResponseBase<SMSResponse>
    {
        public string accountSID;
        public string apiVersion;
        public string errorCode;
        public string errorMessage;
        public string dateCreated;
        public string status;
        public string direction;
    }
}
