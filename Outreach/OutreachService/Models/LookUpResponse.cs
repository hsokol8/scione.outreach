﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SciOne.Utils.Errors.Response;

namespace OutreachService.Models
{
    public class LookUpResponse: ResponseBase<LookUpResponse>
    {
        public string callerName;
        public string countryCode;
        public string carrierName;
        public string numberType;
        public string errorCode;
    }
}
