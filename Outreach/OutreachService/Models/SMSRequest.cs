﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutreachService.Models
{
    public class SMSRequest
    {
        public string PatientPhoneNumber { get; set; }
        public string MessageBody { get; set; }
    }
}
