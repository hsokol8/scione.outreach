﻿using System;
using System.Fabric;
using System.Fabric.Description;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SciOne.Utils.Service;
using OutreachService.Modules;

namespace OutreachService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ServiceEventSource.Current.Message("Beginning Startup");
            ConfigurationSettings settings = FabricRuntime.GetActivationContext().GetConfigurationPackageObject("Config").Settings;
            ServiceEventSource.Current.Message("Adding SciOne");
            services.AddSciOneMvc(ServiceEventSource.Current, typeof(Startup), "OutreachService", new[] { "v1" }, settings);
            ServiceEventSource.Current.Message("Added SciOne");

            try
            {
                // Configure the database
                string connectionString = settings.Sections["Database"].Parameters["ConnectionString"].Value;
                ServiceEventSource.Current.Message("Configuring DB with: {0}", connectionString);
            }
            catch (Exception ex)
            {
                ServiceEventSource.Current.Message("Exception '{0}' configuring DB: {1}", ex.Message, ex);
            }

            services.AddSingleton<MessageHandlerFactory>();
            ServiceEventSource.Current.Message("Finished Startup");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            ServiceEventSource.Current.Message("In Configure, IsDevelopment={0}, IsStaging={1}, IsProduction={2}", env.IsDevelopment(), env.IsStaging(), env.IsProduction());

            app.UseStaticFiles();
            ServiceEventSource.Current.Message("Configuring SciOne");
            app.UseSciOneServices("OutreachService", new[] { "v1" });
            ServiceEventSource.Current.Message("Configured SciOne");
            app.UseMvc();
        }
    }
}
