﻿using System.Diagnostics.Tracing;
using System.Threading.Tasks;

namespace OutreachService
{
    [EventSource(Name = "SciSolutions-OutreachApplication-OutreachService")]
    internal sealed partial class ServiceEventSource : EventSource
    {
        public static readonly ServiceEventSource Current = new ServiceEventSource();

        static ServiceEventSource()
        {
            // A workaround for the problem where ETW activities do not get tracked until Tasks infrastructure is initialized.
            // This problem will be fixed in .NET Framework 4.6.2.
            Task.Run(() => { });
        }

        // Instance constructor is private to enforce singleton semantics
        private ServiceEventSource() : base() { }
    }
}
