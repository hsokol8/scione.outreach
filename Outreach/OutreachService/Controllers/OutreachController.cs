﻿using Microsoft.AspNetCore.Mvc;
using OutreachService.Models;
using OutreachService.Modules;
using OutreachService.Models.SciServer;

namespace OutreachService.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class OutreachController : Controller
    {
        private readonly MessageHandlerFactory _messageHandlerFactory;

        public OutreachController(MessageHandlerFactory messageHandlerFactory)
        {
            _messageHandlerFactory = messageHandlerFactory;
        }

        [HttpPost]
        public ActionResult<SMSResponse> SendInvite([FromBody]SMSRequest request)
        {
            ServiceEventSource.Current.Message("Send Invite Request");
            return _messageHandlerFactory.GetHandler(request).SendMessage(request);
        }

        [HttpPost]
        [Route("lookup")]
        public ActionResult<LookUpResponse> SendLookUp([FromBody]LookUpRequest request)
        {
            ServiceEventSource.Current.Message("Send LookUp Request");
            return _messageHandlerFactory.GetHandler(request).SendMessage(request);
        }

        [HttpPost]
        [Route("email")]
        public ActionResult<EmailResponse> SendLookUp([FromBody]EmailRequest request)
        {
            ServiceEventSource.Current.Message("Send Email Request");
            return _messageHandlerFactory.GetHandler(request).SendMessage(request);
        }
    }
}