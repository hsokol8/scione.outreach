﻿using OutreachService.Models;
using OutreachService.Models.Twilio;
using SciOne.Utils.Api.Request;
using System.Fabric.Description;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serialization.Json;
using System;
using System.Net;

namespace OutreachService.Modules.Handlers
{
    public class TwilioSMSMessageHandler : MessageHandlerBase<SMSRequest, SMSResponse>
    {
        private readonly ConfigurationSettings _settings;
        public TwilioSMSMessageHandler(ConfigurationSettings settings)
        {
            ServiceEventSource.Current.Message("Looking up credentials");
            _settings = settings;

            ServiceEventSource.Current.Message("Found it, looking up server");
            var authConfig = _settings?.Sections["Twilio"];

            AccountSid = authConfig?.Parameters["TwilioSID"]?.Value;
            AuthToken = authConfig?.Parameters["TwilioToken"]?.Value;
            SCIPhoneNumber = authConfig?.Parameters["TwilioPhoneNumber"]?.Value;
            Server = authConfig?.Parameters["TwilioServerURL"]?.Value;

            ServiceEventSource.Current.Message("Found: " + Server);
        }

        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public string SCIPhoneNumber { get; set; }
        public string Server { get; set; }

        public override SMSResponse SendMessage(SMSRequest request)
        {
            // checking that the message body length is less than Twilio's 1600 character max
            bool messageLength = Converter.CheckMeesageLength(request.MessageBody);
            if (!messageLength)
                return SMSResponse.Error("Message body is missing or exceeds the 1600 character maximum", HttpStatusCode.BadRequest);

            // converting any phone number into E.164 format
            var convertedNumber = Converter.NumberConverter(request.PatientPhoneNumber);
            if (convertedNumber == null)
                return SMSResponse.Error("Invalid Phone Number", HttpStatusCode.BadRequest);

            RestClient.BaseUrl = new System.Uri(Server);
            RestClient.Authenticator = new HttpBasicAuthenticator(AccountSid, AuthToken);

            var restRequest = new RestRequest
            {
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };

            restRequest.AddParameter("Body", request.MessageBody);
            restRequest.AddParameter("From", SCIPhoneNumber);
            restRequest.AddParameter("To", convertedNumber);

            // ------------------------------------- USED THE FOLLOWING CODE FROM ELIBIBILITY SERVICE - TRANSUNIONMESSAGEHANDLER.CS, AUTHORS: JAKE CORMIER AND NAVI KAUR -----------------
            var start = DateTime.Now;
            string error;

            try
            {
                var restResponse = ExecuteWithPolicy(restRequest);

                if (restResponse == null)
                {
                    error = "response is null";
                }
                else if (!string.IsNullOrWhiteSpace(restResponse.ErrorMessage))
                {
                    error = restResponse.StatusCode + " : " + restResponse.ErrorMessage;
                    ServiceEventSource.Current.Message(
                        $"Failed in {DateTime.Now.Millisecond - start.Millisecond} ms, ResponseStatus={restResponse.ResponseStatus}, ErrorMessage={restResponse.ErrorMessage}\r\n{restResponse.ErrorException?.StackTrace}");
                }
                else if (string.IsNullOrWhiteSpace(restResponse.Content))
                {
                    error = "response.Content is empty or null";
                    ServiceEventSource.Current.Message(
                        $"Response.Content is null in {DateTime.Now.Millisecond - start.Millisecond} ms, ResponseStatus={restResponse.ResponseStatus}, ErrorMessage={restResponse.ErrorMessage}\r\n{restResponse.ErrorException?.StackTrace}");
                }
                else
                {
                    ServiceEventSource.Current.Message("response.Content=" + restResponse.Content);
                    var deserialize = new JsonDeserializer();
                    var twilioResponse = deserialize.Deserialize<TwilioResponse>(restResponse);

                    if (twilioResponse.Status == "404")
                        return SMSResponse.Error((twilioResponse.ErrorMessage + $" Twilio Error Code: {twilioResponse.ErrorCode}"), HttpStatusCode.NotFound);

                    if (twilioResponse.Status == "401")
                        return SMSResponse.Error((twilioResponse.ErrorMessage + $" Twilio Error Code: {twilioResponse.ErrorCode}"), HttpStatusCode.Unauthorized);

                    return Converter.ConvertResponse(new SMSResponse(), twilioResponse);
                }
            }
            catch (Exception ex)
            {
                ServiceEventSource.Current.Message(
                    $"Exception in {DateTime.Now.Millisecond - start.Millisecond} ms, Exception={ex}");
                error = ex.Message;
                if (ex.InnerException != null)
                    ServiceEventSource.Current.Message(
                        $"InnerException={ex.InnerException.Message}, StackTrace={ex.InnerException.StackTrace}");
            }

            return SMSResponse.Error(error, HttpStatusCode.InternalServerError);

            // ---------------------------------------------------------------------------  END OF DUPLIATED CODE ------------------------------------------------------------------------------
        }
    }
}
