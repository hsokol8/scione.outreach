﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OutreachService.Models;
using OutreachService.Models.SciServer;
using SciOne.Utils.Api.Request;
using System.Fabric.Description;
using MailKit.Net.Smtp; 
using MimeKit;
using System.Net.NetworkInformation;


namespace OutreachService.Modules.Handlers
{
    public class EmailMessageHandler: MessageHandlerBase<EmailRequest, EmailResponse>
    {
        private readonly ConfigurationSettings _settings;
        public EmailMessageHandler(ConfigurationSettings settings)
        {
            ServiceEventSource.Current.Message("Looking up credentials");
            _settings = settings;

            ServiceEventSource.Current.Message("Found it, looking up server");
            var authConfig = _settings?.Sections["Email"];

            FromEmail = authConfig?.Parameters["FromEmail"]?.Value;
            Server = authConfig?.Parameters["EmailServer"]?.Value;

            ServiceEventSource.Current.Message("Found: " + Server);
        }

        public string FromEmail { get; set; }
        public string Server { get; set; }

        public override EmailResponse SendMessage(EmailRequest request)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(FromEmail));
            message.To.Add(new MailboxAddress(request.PatientEmail));
            message.Body = new TextPart("plain") { Text = request.MessageBody };
            message.Subject = request.EmailSubject;

            SmtpClient client = new SmtpClient();
            client.AuthenticationMechanisms.Remove("XOAUTH2");
            client.Connect(Server, 587, false);
            client.Send(message);
            client.Disconnect(true);
            
                return new EmailResponse();
        }
    }
}
