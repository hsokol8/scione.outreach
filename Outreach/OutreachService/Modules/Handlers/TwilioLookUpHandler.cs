﻿using System;
using OutreachService.Models;
using OutreachService.Models.Twilio;
using SciOne.Utils.Api.Request;
using System.Fabric.Description;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serialization.Json;
using System.Net;


namespace OutreachService.Modules.Handlers
{
    public class TwilioLookUpHandler : MessageHandlerBase<LookUpRequest, LookUpResponse>
    {

        private readonly ConfigurationSettings _settings;
        public TwilioLookUpHandler(ConfigurationSettings settings)
        {
            ServiceEventSource.Current.Message("Looking up credentials");
            _settings = settings;

            ServiceEventSource.Current.Message("Found it, looking up server");
            var authConfig = _settings?.Sections["Twilio"];

            AccountSid = authConfig?.Parameters["TwilioSID"]?.Value;
            AuthToken = authConfig?.Parameters["TwilioToken"]?.Value;

            authConfig = _settings?.Sections["LookUp"];
            LookUpServer = authConfig?.Parameters["TwilioLookUpServer"]?.Value;
            ServiceEventSource.Current.Message("Found: " + LookUpServer);
        }

        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public string LookUpServer { get; set; }

        public override LookUpResponse SendMessage(LookUpRequest request)
        {               
            var number = Converter.NumberConverter(request.PatientPhoneNumber);
            if (number == null)
                return LookUpResponse.Error("Invalid Phone Number", HttpStatusCode.BadRequest);

            string newServer = LookUpServer + number + ("?Type=carrier");

            RestClient.BaseUrl = new System.Uri(newServer);
            RestClient.Authenticator = new HttpBasicAuthenticator(AccountSid, AuthToken);

            var restRequest = new RestRequest
            {
                Method = Method.GET,
                RequestFormat = DataFormat.Json
            };

            var start = DateTime.Now;
            string error;

            try
            {
                var restResponse = ExecuteWithPolicy(restRequest);

                if (restResponse == null)
                {
                    error = "response is null";
                }
                else if (!string.IsNullOrWhiteSpace(restResponse.ErrorMessage))
                {
                    error = restResponse.StatusCode + " : " + restResponse.ErrorMessage;
                    ServiceEventSource.Current.Message(
                        $"Failed in {DateTime.Now.Millisecond - start.Millisecond} ms, ResponseStatus={restResponse.ResponseStatus}, ErrorMessage={restResponse.ErrorMessage}\r\n{restResponse.ErrorException?.StackTrace}");
                }
                else if (string.IsNullOrWhiteSpace(restResponse.Content))
                {
                    error = "response.Content is empty or null";
                    ServiceEventSource.Current.Message(
                        $"Response.Content is null in {DateTime.Now.Millisecond - start.Millisecond} ms, ResponseStatus={restResponse.ResponseStatus}, ErrorMessage={restResponse.ErrorMessage}\r\n{restResponse.ErrorException?.StackTrace}");
                }
                else
                {
                    if (restResponse.Content[3] == 'o')
                    {
                        ServiceEventSource.Current.Message("response.Content=" + restResponse.Content);
                        var deserialize = new JsonDeserializer();
                        var twilioErroResponse = deserialize.Deserialize<LUErrorResponse>(restResponse);

                        ServiceEventSource.Current.Message($"Error Code: {twilioErroResponse.Code} Error Message: {twilioErroResponse.Message} " +
                            $"Error Details: {twilioErroResponse.MoreInfo} HTTP Status: {twilioErroResponse.Status}");

                        if (twilioErroResponse.Status == "404")
                            return LookUpResponse.Error((twilioErroResponse.Message + $" Twilio Error Code: {twilioErroResponse.Code}"), HttpStatusCode.NotFound);

                        if (twilioErroResponse.Status == "401")
                            return LookUpResponse.Error((twilioErroResponse.Message + $" Twilio Error Code: {twilioErroResponse.Code}"), HttpStatusCode.Unauthorized);

                        return LookUpResponse.Error(twilioErroResponse.Message, HttpStatusCode.BadRequest);
                    }
                    else
                    {
                        ServiceEventSource.Current.Message("response.Content=" + restResponse.Content);
                        var deserialize = new JsonDeserializer();
                        var twilioLUResponse = deserialize.Deserialize<LUResponse>(restResponse);
                        ServiceEventSource.Current.Message($"Carrier Name: {twilioLUResponse.Carrier.Name} Country Code: {twilioLUResponse.CountryCode} Customer Name: {twilioLUResponse.CallerName} " +
                            $"Error Code: {twilioLUResponse.Carrier.ErrorCode} Phone Number Type: {twilioLUResponse.Carrier.Type} MNC: {twilioLUResponse.Carrier.MobileNetworkCode}");
                        return Converter.ConvertResponse(new LookUpResponse(), twilioLUResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                ServiceEventSource.Current.Message(
                    $"Exception in {DateTime.Now.Millisecond - start.Millisecond} ms, Exception={ex}");
                error = ex.Message;
                if (ex.InnerException != null)
                    ServiceEventSource.Current.Message(
                        $"InnerException={ex.InnerException.Message}, StackTrace={ex.InnerException.StackTrace}");
            }

            return LookUpResponse.Error(error, HttpStatusCode.InternalServerError);
        }
    }
}
