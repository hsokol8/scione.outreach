﻿using OutreachService.Modules.Handlers;
using OutreachService.Models;
using OutreachService.Models.SciServer;
using System.Fabric.Description;
using SciOne.Utils.Api.Request;

namespace OutreachService.Modules
{
    public class MessageHandlerFactory
    {
        private readonly ConfigurationSettings _configurationSetting;

        public MessageHandlerFactory(ConfigurationSettings configurationSetting)
        {
            _configurationSetting = configurationSetting;
        }

        public MessageHandlerBase<SMSRequest, SMSResponse> GetHandler(SMSRequest request)
        {
            ServiceEventSource.Current.Message("You are getting sent to the SMS Message Handler");
            return new TwilioSMSMessageHandler(_configurationSetting);
        }

        public MessageHandlerBase<LookUpRequest, LookUpResponse> GetHandler(LookUpRequest request)
        {
            ServiceEventSource.Current.Message("You are getting sent to the Look Up Handler");
            return new TwilioLookUpHandler(_configurationSetting);
        }

        public MessageHandlerBase<EmailRequest, EmailResponse> GetHandler(EmailRequest request)
        {
            ServiceEventSource.Current.Message("You are getting sent to the Email Handler");
            return new EmailMessageHandler(_configurationSetting);
        }
    }
}
