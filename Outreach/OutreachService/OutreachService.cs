﻿using System;
using System.Collections.Generic;
using System.Fabric;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.ServiceFabric.Services.Communication.AspNetCore;
using Microsoft.ServiceFabric.Services.Communication.Runtime;
using Microsoft.ServiceFabric.Services.Runtime;

namespace OutreachService
{
    /// <summary>
    /// The FabricRuntime creates an instance of this class for each service type instance. 
    /// </summary>
    internal sealed class OutreachService : StatelessService
    {
        public OutreachService(StatelessServiceContext context)
            : base(context)
        { }

        /// <summary>
        /// Optional override to create listeners (like tcp, http) for this service instance.
        /// </summary>
        /// <returns>The collection of listeners.</returns>
        protected override IEnumerable<ServiceInstanceListener> CreateServiceInstanceListeners()
        {
            return new ServiceInstanceListener[]
            {
                new ServiceInstanceListener(serviceContext =>
                    new KestrelCommunicationListener(serviceContext, "ServiceEndpoint", (url, listener) =>
                    {
                        ServiceEventSource.Current.SetServiceContext(serviceContext);
                        ServiceEventSource.Current.ServiceMessage(serviceContext, $"Starting Kestrel on {url}");

                        // Assumes only one Endpoint (see [0] reference), modify if more than one Endpoint defined.
                        return new WebHostBuilder()
                                    .UseKestrel(/*(options =>
                                        options.Listen(IPAddress.Any, serviceContext.CodePackageActivationContext.GetEndpoints()[0].Port,
                                            listenOptions => listenOptions.UseHttps(FindCertificate(serviceContext, serviceEventSource))))*/)
                                    .ConfigureServices(
                                        services => services
                                            .AddSingleton(serviceContext))
                                    .UseContentRoot(Directory.GetCurrentDirectory())
                                    .UseStartup<Startup>()
                                    .UseServiceFabricIntegration(listener, ServiceFabricIntegrationOptions.None)
                                    .UseUrls(url)
                                    .Build();
                    }))
            };
        }

        private X509Certificate2 FindCertificate(ServiceContext serviceContext, ServiceEventSource serviceEventSource)
        {
            var sslCertHash = serviceContext.CodePackageActivationContext.GetConfigurationPackageObject("Config").Settings.Sections["SSL"].Parameters["SslCertHash"].Value;
            var findType = sslCertHash.Length < 30 ? X509FindType.FindByIssuerName : X509FindType.FindByThumbprint;
            serviceEventSource.Message("Certificate {0}:'{1}'", findType.ToString(), sslCertHash);
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            var certificateList = store.Certificates.Find(findType, sslCertHash, false).GetEnumerator();
            if (!certificateList.MoveNext())
            {
                serviceEventSource.ServiceMessage(serviceContext, "Service startup failed: Certificate not found");
                throw new Exception("Certificate not found");
            }

            return certificateList.Current;
        }
    }
}
