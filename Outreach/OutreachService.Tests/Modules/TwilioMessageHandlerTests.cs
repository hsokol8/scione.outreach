﻿using System;
using System.IO;
using System.Fabric.Description;
using Newtonsoft.Json;
using NUnit.Framework;
using static ServiceFabric.Mocks.MockConfigurationPackage;
using OutreachService.Models;
using OutreachService.Modules.Handlers;

namespace OutreachService.Tests.Modules
{
    class TwilioMessageHandlerTests
    {
        private ConfigurationSettings settings;
        private TwilioSMSMessageHandler handler;

        [SetUp]
        public void SetUp()
        {
            var configSections = new ConfigurationSectionCollection();
            settings = CreateConfigurationSettings(configSections);
            var configSection = CreateConfigurationSection("Twilio");
            configSections.Add(configSection);
            configSection.Parameters.Add(CreateConfigurationSectionParameters("TwilioServerURL", "http://www.test.com"));
            configSection.Parameters.Add(CreateConfigurationSectionParameters("TwilioSID", "TestUser"));
            configSection.Parameters.Add(CreateConfigurationSectionParameters("TwilioToken", "TestPass"));
            configSection.Parameters.Add(CreateConfigurationSectionParameters("TwilioPhoneNumber", "+12345678910"));
            configSection.Parameters.Add(CreateConfigurationSectionParameters("TwilioLookUpServer", "http://anothertest.com"));
            handler = new TwilioSMSMessageHandler(settings);

        }

        [Test]
        public void TestingConfigs()
        {
            var request = JsonConvert.DeserializeObject<SMSRequest>(File.ReadAllText("..\\..\\..\\Resources\\ValidRequests.txt"));

            Assert.That(request.MessageBody, Is.EqualTo("Test Message"));
            Assert.That(request.PatientPhoneNumber, Is.EqualTo("2068184530"));
            Assert.That(handler.SCIPhoneNumber, Is.EqualTo("+12345678910"));
            Assert.That(handler.AccountSid, Is.EqualTo("TestUser"));
            Assert.That(handler.AuthToken, Is.EqualTo("TestPass"));
            Assert.That(handler.Server, Is.EqualTo("http://www.test.com"));
        }
    }
}
