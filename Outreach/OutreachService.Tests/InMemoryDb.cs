﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

public static class InMemoryDbExtensions
{
    public static DbContextOptions<TContext> AsInMemoryDb<TContext>(this string databaseName) where TContext : DbContext
    {
        var options = new DbContextOptionsBuilder<TContext>()
                .UseInMemoryDatabase(databaseName: databaseName)
                .Options;
        return options;
    }

    public static IEnumerable<T> InitializeDb<T, TContext>(this TContext context, IEnumerable<T> list) where T : class where TContext : DbContext
    {
        foreach (var item in list)
        {
            context.Add<T>(item);
        }
        context.SaveChanges();
        return list;
    }

    public static T[] InitializeDb<T, TContext>(this TContext context, T[] elements) where T : class where TContext : DbContext
    {
        foreach (var item in elements)
        {
            context.Add<T>(item);
        }
        context.SaveChanges();
        return elements;
    }
}