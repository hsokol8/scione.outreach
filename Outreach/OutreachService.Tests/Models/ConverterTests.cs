﻿using System;
using NUnit.Framework;
using OutreachService.Models.Twilio;
using System.IO;
using Newtonsoft.Json;
using OutreachService.Models;

namespace OutreachService.Tests.Models
{
    class ConverterTests
    {
        [Test]
        public void PhoneNumberConversions()
        {

            var request1 = Converter.NumberConverter("2062355689");
            Assert.That(request1, Is.EqualTo("+12062355689"));

            request1 = Converter.NumberConverter("+12062355689");
            Assert.That(request1, Is.EqualTo("+12062355689"));

            request1 = Converter.NumberConverter("12062355689");
            Assert.That(request1, Is.EqualTo("+12062355689"));

            request1 = Converter.NumberConverter("2355689");
            Assert.That(request1, Is.Null);

            request1 = Converter.NumberConverter("1235568999");
            Assert.That(request1, Is.Null);

            request1 = Converter.NumberConverter("0235568999");
            Assert.That(request1, Is.Null);

            request1 = Converter.NumberConverter("xxxxxx");
            Assert.That(request1, Is.Null);
        }

        [Test]
        public void ResponseConverterTest()
        {
            var jsonText = File.ReadAllText("..\\..\\..\\Resources\\ValidResponse.txt");
            var response = JsonConvert.DeserializeObject<TwilioResponse>(jsonText);

            var inviteResponse = Converter.ConvertResponse(new SMSResponse(), response);

            Assert.That(inviteResponse.apiVersion, Is.EqualTo("2010-04-01"));
            Assert.That(inviteResponse.direction, Is.EqualTo("outbound-api"));
            Assert.That(inviteResponse.accountSID, Is.EqualTo("AC047a9c0b9e464c6d87264883e839823e"));
            Assert.That(inviteResponse.errorCode, Is.Null);
            Assert.That(inviteResponse.errorMessage, Is.Null);
            Assert.That(inviteResponse.status, Is.EqualTo("sent"));
            Assert.That(inviteResponse.dateCreated, Is.EqualTo("Thu, 30 Jul 2015 20:12:31 +0000"));
        }

        [Test]
        public void BodyMessageCapacityTest()
        {
            var validMessage = JsonConvert.DeserializeObject<SMSRequest>(File.ReadAllText("..\\..\\..\\Resources\\ValidRequests.txt"));
            var invalidMessage = JsonConvert.DeserializeObject<SMSRequest>(File.ReadAllText("..\\..\\..\\Resources\\InvalidMessage.txt"));

            bool okMessageLength = Converter.CheckMeesageLength(validMessage.MessageBody);
            Assert.That(okMessageLength, Is.True);

            okMessageLength = Converter.CheckMeesageLength(invalidMessage.MessageBody);
            Assert.That(okMessageLength, Is.False);
        }

        [Test]
        public void LookUpConverterTest()
        {
            var twilioLookUpResponse = JsonConvert.DeserializeObject<LUResponse>(File.ReadAllText("..\\..\\..\\Resources\\ValidLookUpResponse.txt"));
            Console.WriteLine(twilioLookUpResponse.Carrier.Name);
            var answer = Converter.ConvertResponse(new LookUpResponse(), twilioLookUpResponse);

            Assert.That(answer.callerName, Is.Null);
            Assert.That(answer.carrierName, Is.EqualTo("T-Mobile USA, Inc."));
            Assert.That(answer.countryCode, Is.EqualTo("US"));
            Assert.That(answer.errorCode, Is.Null);
            Assert.That(answer.numberType, Is.EqualTo("mobile"));
        }
    }
}
