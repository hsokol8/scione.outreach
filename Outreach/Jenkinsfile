import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
def gitprops = []
pipeline {
	agent { node { 
			label 'SF'
			customWorkspace "${env.JENKINS_WORKSPACEDIR}/${env.JOB_NAME}".replace('%2F','-')
			}
	}
	environment {
		BUILDMASTER_PACKAGE_NUMBER=null		
	}
	options {
		disableConcurrentBuilds()
		buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '10', daysToKeepStr: '', numToKeepStr: '60'))
		timestamps()
	}

	stages {
		stage ('Load Build Properties') {
			steps {
				load 'JenkinsBuildParams.groovy'
			}
		}
		
		stage ('Build') {
			steps {
				script {
					def stdout = powershell(returnStdout: true, script: '.\\build.ps1')
					println stdout
					
					try {
					    echo "INFO: Loading GitVersion Properties."
						gitprops = readProperties file:'gitVersion.properties'
					} catch (e) {
						echo "${e}"
					}
				}
			}
		}

		stage ('Run Unit Tests') {
			steps {
				script {
					def stdout = powershell(returnStdout: true, script: '.\\build.ps1 -target run-unittests')
					println stdout
				}
			}
			post {
				always {
					xunit testTimeMargin: '3000', thresholdMode: 1, thresholds: [], tools: [MSTest(deleteOutputFiles: true, failIfNotNew: false, pattern: "${env.PROJ_NAME}Service.Tests\\TestResults\\TestResults.xml", skipNoTestFiles: true, stopProcessingIfError: false)]
				}
			}
		}
		
		stage ('Archive Artifacts')
		{
			when {
				expression {BRANCH_NAME ==~ /(testing|develop|hotfix|master\/*)|(release\/.+)/}
			}				
			steps {
				archiveArtifacts allowEmptyArchive: false, artifacts: '**/*', caseSensitive: false, defaultExcludes: true, fingerprint: false, onlyIfSuccessful: false 
			}					
		}

		stage ('Create deployment package') {
			when {
				expression {BRANCH_NAME ==~ /(testing|develop|hotfix|master\/*)|(release\/.+)/}
			}
			steps {
				script {
					def stdout = powershell(returnStdOut: true, script: '''
						set-location $env:SCRIPTS_PATH
						$src = join-path "$env:WORKSPACE" "$env:PROJ_NAME\\pkg"
						$dest = join-path "$env:WORKSPACE" "deploymentpkg"
						$upackcfg = join-path "$env:WORKSPACE" "upack.json"
						$appParamFolder = join-path "$env:WORKSPACE" "$env:PROJ_NAME\\ApplicationParameters"
						$gitverfile = join-path "$env:WORKSPACE" "gitVersion.properties"
						$gitprops = convertfrom-stringdata (gc $gitverfile -raw)
						$ver = $gitprops.GitVersion_FullSemVer
						
						. .\\ServiceFabricUPackMgr.ps1;CreatePkg -srcFolder $src -targetDir $dest -AppParamFolder $appParamFolder -UpackCfgFile $upackcfg -pkgVer $ver -feedGroup SCIOne -uPackTool $env:UPACK_EXE
						
						set-location $env:WORKSPACE            
					''')
					println stdout
				}
			}
		}

		stage ('Upload deployment package')
		{
			when {
				expression {fileExists ("\\deploymentpkg\\${env.PROJ_NAME}-${gitprops.GitVersion_MajorMinorPatch}.upack")}
			}
			
			steps {
				script {
					def stdout = powershell(returnStdOut: true, script: '''
						set-location $env:SCRIPTS_PATH
						$gitverfile = join-path "$env:WORKSPACE" "gitVersion.properties"
						$gitprops = convertfrom-stringdata (gc $gitverfile -raw)
						$ver = $gitprops.GitVersion_MajorMinorPatch
						$pkgFileName = "$env:PROJ_NAME-"+ $ver +".upack"
						$pkgPath = join-path "$env:WORKSPACE" "deploymentpkg" | join-path -ChildPath $pkgFileName

						. .\\ServiceFabricUPackMgr.ps1;PushPkg -Package $pkgPath -FeedUrl $env:BUILD_ARTIFACT_FEED -uPackTool $env:UPACK_EXE

						set-location $env:WORKSPACE				
					''')
					println stdout
				}
			}
		}

		stage ('Create Buildmaster package')
		{
			when {
				expression {BRANCH_NAME ==~ /(testing|develop|hotfix|master\/*)|(release\/.+)/}
			}
				steps {
					buildMasterWithApplicationRelease(applicationId: "${env.BuildMaster_appId}", deployableId: "${env.BuildMaster_deployableId}") {
						script {
							def (reponame,jobname) = JOB_NAME.split('/')
							def bmjenkinsjobname= "${reponame}/job/${jobname}"
							BUILDMASTER_PACKAGE_NUMBER = buildMasterCreatePackage(applicationId: BUILDMASTER_APPLICATION_ID,
								deployToFirstStage:[waitUntilDeploymentCompleted: true,printLogOnFailure: false],
								packageNumber: BUILDMASTER_PACKAGE_NUMBER,
								releaseNumber: "${gitprops.GitVersion_MajorMinorPatch}", 
								packageVariables: [preserveVariables: false, 
									variables: "JenkinsBuildNumber=$BUILD_NUMBER\nBuildVersion=${gitprops.GitVersion_FullSemVer}\nJenkinsProject=$bmjenkinsjobname"])			
						}
					}
			}
		}

		stage ('Deployment')
		{
			when {
				expression {BRANCH_NAME ==~ /(testing|develop|hotfix|master\/*)|(release\/.+)/}
			}
			stages {
				stage ('Deploy to Dev Cluster') {
					when {
						branch 'testing'
					}
					steps {					
						buildMasterWithApplicationRelease(applicationId: "${env.BuildMaster_appId}", deployableId: "${env.BuildMaster_deployableId}")  {
							buildMasterDeployPackageToStage(stage: 'Development',
								applicationId: BUILDMASTER_APPLICATION_ID,
								releaseNumber: "${gitprops.GitVersion_MajorMinorPatch}",
								packageNumber: BUILDMASTER_PACKAGE_NUMBER,
								waitUntilDeploymentCompleted: true,
								printLogOnFailure: false)
						}
					}
				}
				stage ('Deploy to QA Cluster') {
					when {
						expression {BRANCH_NAME ==~ /(develop|hotfix|master\/*)|(release\/.+)/}
					}
					steps {					
						buildMasterWithApplicationRelease(applicationId: "${env.BuildMaster_appId}", deployableId: "${env.BuildMaster_deployableId}")  {
							buildMasterDeployPackageToStage(stage: 'QA Testing',
								applicationId: BUILDMASTER_APPLICATION_ID,
								releaseNumber: "${gitprops.GitVersion_MajorMinorPatch}",
								packageNumber: BUILDMASTER_PACKAGE_NUMBER,
								waitUntilDeploymentCompleted: true,
								printLogOnFailure: false)
						}
					}
				}				
			}
		}
		
		stage ('Run Integration Tests')
		{
			when {
				allOf {
					not {environment name:'run_integrationtests', value:'0'}
					expression {BRANCH_NAME ==~ /(testing|develop|hotfix|master\/*)|(release\/.+)/}
				}
			}	
			steps{
				script {
					def stdout = powershell(returnStdOut: true, script: '''
						if ($env:BRANCH_NAME -ne 'develop')
						{
							. .\\build.ps1 -target run-integration-tests-qa
						}
						else
						{
							. .\\build.ps1 -target run-integration-tests
						}
						exit 0
					''')
					println stdout

				}
			}
			post {
				always {
					junit testResults: "${env.PROJ_NAME}_IntegrationTests.xml", allowEmptyResults: true
				}
			}
		}
	}
	
	post {
		always {
		    script {
				currentBuild.displayName = gitprops.GitVersion_FullSemVer
			}
		}
		failure {
			script {
				if (Notifications_Enabled != '0')
				{
					// Send Slack msg
					def testSummary = getTestSummary()
					def jname = env.JOB_NAME.replace('%2F','/')
					def fbstr = "FAILURE: Job ${jname} ${env.BUILD_DISPLAY_NAME}"
					def pretxt = "*Failure*: Job ${jname}  ${env.BUILD_DISPLAY_NAME}"
					def title = "${jname}  ${env.BUILD_DISPLAY_NAME}"
					def titlelink = "${env.RUN_DISPLAY_URL}"
					def txt = "Version: ${env.BUILD_DISPLAY_NAME}\nCommit: ${env.GIT_COMMIT}\n<${env.RUN_CHANGES_DISPLAY_URL}|Changes>\n${testSummary}" 
					
					JSONArray attachments = new JSONArray();
					JSONObject attachment = new JSONObject();						
						
					attachment.put('fallback',fbstr.toString());
					attachment.put('color','#FF0000');
					attachment.put('pretext',pretxt.toString());
					attachment.put('title',title.toString());
					attachment.put('title_link',titlelink.toString());
					attachment.put('text',txt.toString());

					attachments.add(attachment);
					
					slackSend (channel:'scione-notifications',color: '#FF0000',notify: true, attachments: attachments.toString())

					if (env.email_list != '') 
					{					
						// Send Email
						def recps = env.email_list
						def gitProps= readProperties file:'gitVersion.properties'
						def bver = gitprops.GitVersion_FullSemVer
						def subj = jname + " - "+ currentBuild.currentResult +" - Build "+ bver
						def content = "Build Failure - "+ env.BUILD_URL
						emailext to: recps, subject: subj, body: content
					}					
				}
			}
		}
		unstable {
			script {
				if (Notifications_Enabled != '0')
				{
					def testSummary = getTestSummary()
					def jname = env.JOB_NAME.replace('%2F','/')
					def fbstr = "UNSTABLE: Job ${jname} ${env.BUILD_DISPLAY_NAME}"
					def pretxt = "*Unstable*: Job ${jname} ${env.BUILD_DISPLAY_NAME}"
					def title = "${jname} ${env.BUILD_DISPLAY_NAME}"
					def titlelink = "${env.RUN_DISPLAY_URL}"
					def txt = "Version: ${env.BUILD_DISPLAY_NAME}\nCommit: ${env.GIT_COMMIT}\n<${env.RUN_CHANGES_DISPLAY_URL}|Changes>\n${testSummary}" 
					
					JSONArray attachments = new JSONArray();
					JSONObject attachment = new JSONObject();						
						
					attachment.put('fallback',fbstr.toString());
					attachment.put('color','#FFFF00');
					attachment.put('pretext',pretxt.toString());
					attachment.put('title',title.toString());
					attachment.put('title_link',titlelink.toString());
					attachment.put('text',txt.toString());
					
					attachments.add(attachment);
					
					slackSend (channel:'scione-notifications',color: '#FFFF00',notify: true, attachments: attachments.toString())							
				}
			}
		}
		success {
			script {
				if (Notifications_Enabled != '0')
				{
					def testSummary = getTestSummary()
					def jname = env.JOB_NAME.replace('%2F','/')
					def fbstr = "SUCCESSFUL: Job ${jname} ${env.BUILD_DISPLAY_NAME}"
					def pretxt = "*Successful*: Job ${jname} ${env.BUILD_DISPLAY_NAME}"
					def title = "${jname} ${env.BUILD_DISPLAY_NAME}"
					def titlelink = "${env.RUN_DISPLAY_URL}"
					def txt = "Version: ${env.BUILD_DISPLAY_NAME}\nCommit: ${env.GIT_COMMIT}\n<${env.RUN_CHANGES_DISPLAY_URL}|Changes>\n${testSummary}" 
					
					JSONArray attachments = new JSONArray();
					JSONObject attachment = new JSONObject();						
						
					attachment.put('fallback',fbstr.toString());
					attachment.put('color','#36a64f');
					attachment.put('pretext',pretxt.toString());
					attachment.put('title',title.toString());
					attachment.put('title_link',titlelink.toString());
					attachment.put('text',txt.toString());

					attachments.add(attachment);
					
					slackSend (channel:'scione-notifications',color: '#36a64f',notify: true, attachments: attachments.toString())	
				}
			}			
		}
	}		
}