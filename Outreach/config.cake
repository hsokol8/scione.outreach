var applicationName = "Outreach";
var jiraCode = "XXX";
var hasUnitTests=true;
var hasIntegrationTests=true;
var netCoreVersion = "netcoreapp2.1";

public static class FabricConfig
{
    public static string Package => "Microsoft.VisualStudio.Azure.Fabric.MSBuild";
    public static string Version => "1.6.7";
}
